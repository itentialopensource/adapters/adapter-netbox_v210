/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-netbox_v210',
      type: 'NetboxV210',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const NetboxV210 = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Netbox_v210 Adapter Test', () => {
  describe('NetboxV210 Class Tests', () => {
    const a = new NetboxV210(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('netbox_v210'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('netbox_v210'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('NetboxV210', pronghornDotJson.export);
          assert.equal('Netbox_v210', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-netbox_v210', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('netbox_v210'));
          assert.equal('NetboxV210', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-netbox_v210', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-netbox_v210', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getCircuitsCircuitTerminations - errors', () => {
      it('should have a getCircuitsCircuitTerminations function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuitTerminations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCircuitsCircuitTerminations - errors', () => {
      it('should have a postCircuitsCircuitTerminations function', (done) => {
        try {
          assert.equal(true, typeof a.postCircuitsCircuitTerminations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postCircuitsCircuitTerminations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postCircuitsCircuitTerminations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsCircuitTerminations - errors', () => {
      it('should have a putCircuitsCircuitTerminations function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsCircuitTerminations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsCircuitTerminations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putCircuitsCircuitTerminations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsCircuitTerminations - errors', () => {
      it('should have a patchCircuitsCircuitTerminations function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsCircuitTerminations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsCircuitTerminations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchCircuitsCircuitTerminations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuitTerminations - errors', () => {
      it('should have a deleteCircuitsCircuitTerminations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsCircuitTerminations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitTerminationsId - errors', () => {
      it('should have a getCircuitsCircuitTerminationsId function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuitTerminationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getCircuitsCircuitTerminationsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getCircuitsCircuitTerminationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsCircuitTerminationsId - errors', () => {
      it('should have a putCircuitsCircuitTerminationsId function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsCircuitTerminationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putCircuitsCircuitTerminationsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putCircuitsCircuitTerminationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsCircuitTerminationsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putCircuitsCircuitTerminationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsCircuitTerminationsId - errors', () => {
      it('should have a patchCircuitsCircuitTerminationsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsCircuitTerminationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchCircuitsCircuitTerminationsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchCircuitsCircuitTerminationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsCircuitTerminationsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchCircuitsCircuitTerminationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuitTerminationsId - errors', () => {
      it('should have a deleteCircuitsCircuitTerminationsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsCircuitTerminationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteCircuitsCircuitTerminationsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteCircuitsCircuitTerminationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitTerminationsIdTrace - errors', () => {
      it('should have a getCircuitsCircuitTerminationsIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuitTerminationsIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getCircuitsCircuitTerminationsIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getCircuitsCircuitTerminationsIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitTypes - errors', () => {
      it('should have a getCircuitsCircuitTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuitTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCircuitsCircuitTypes - errors', () => {
      it('should have a postCircuitsCircuitTypes function', (done) => {
        try {
          assert.equal(true, typeof a.postCircuitsCircuitTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postCircuitsCircuitTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postCircuitsCircuitTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsCircuitTypes - errors', () => {
      it('should have a putCircuitsCircuitTypes function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsCircuitTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsCircuitTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putCircuitsCircuitTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsCircuitTypes - errors', () => {
      it('should have a patchCircuitsCircuitTypes function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsCircuitTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsCircuitTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchCircuitsCircuitTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuitTypes - errors', () => {
      it('should have a deleteCircuitsCircuitTypes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsCircuitTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitTypesId - errors', () => {
      it('should have a getCircuitsCircuitTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuitTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getCircuitsCircuitTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getCircuitsCircuitTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsCircuitTypesId - errors', () => {
      it('should have a putCircuitsCircuitTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsCircuitTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putCircuitsCircuitTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putCircuitsCircuitTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsCircuitTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putCircuitsCircuitTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsCircuitTypesId - errors', () => {
      it('should have a patchCircuitsCircuitTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsCircuitTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchCircuitsCircuitTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchCircuitsCircuitTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsCircuitTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchCircuitsCircuitTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuitTypesId - errors', () => {
      it('should have a deleteCircuitsCircuitTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsCircuitTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteCircuitsCircuitTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteCircuitsCircuitTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuits - errors', () => {
      it('should have a getCircuitsCircuits function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCircuitsCircuits - errors', () => {
      it('should have a postCircuitsCircuits function', (done) => {
        try {
          assert.equal(true, typeof a.postCircuitsCircuits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postCircuitsCircuits(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postCircuitsCircuits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsCircuits - errors', () => {
      it('should have a putCircuitsCircuits function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsCircuits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsCircuits(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putCircuitsCircuits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsCircuits - errors', () => {
      it('should have a patchCircuitsCircuits function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsCircuits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsCircuits(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchCircuitsCircuits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuits - errors', () => {
      it('should have a deleteCircuitsCircuits function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsCircuits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitsId - errors', () => {
      it('should have a getCircuitsCircuitsId function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuitsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getCircuitsCircuitsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getCircuitsCircuitsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsCircuitsId - errors', () => {
      it('should have a putCircuitsCircuitsId function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsCircuitsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putCircuitsCircuitsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putCircuitsCircuitsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsCircuitsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putCircuitsCircuitsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsCircuitsId - errors', () => {
      it('should have a patchCircuitsCircuitsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsCircuitsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchCircuitsCircuitsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchCircuitsCircuitsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsCircuitsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchCircuitsCircuitsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuitsId - errors', () => {
      it('should have a deleteCircuitsCircuitsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsCircuitsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteCircuitsCircuitsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteCircuitsCircuitsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsProviders - errors', () => {
      it('should have a getCircuitsProviders function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCircuitsProviders - errors', () => {
      it('should have a postCircuitsProviders function', (done) => {
        try {
          assert.equal(true, typeof a.postCircuitsProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postCircuitsProviders(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postCircuitsProviders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsProviders - errors', () => {
      it('should have a putCircuitsProviders function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsProviders(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putCircuitsProviders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsProviders - errors', () => {
      it('should have a patchCircuitsProviders function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsProviders(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchCircuitsProviders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsProviders - errors', () => {
      it('should have a deleteCircuitsProviders function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsProvidersId - errors', () => {
      it('should have a getCircuitsProvidersId function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsProvidersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getCircuitsProvidersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getCircuitsProvidersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsProvidersId - errors', () => {
      it('should have a putCircuitsProvidersId function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsProvidersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putCircuitsProvidersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putCircuitsProvidersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsProvidersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putCircuitsProvidersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsProvidersId - errors', () => {
      it('should have a patchCircuitsProvidersId function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsProvidersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchCircuitsProvidersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchCircuitsProvidersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsProvidersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchCircuitsProvidersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsProvidersId - errors', () => {
      it('should have a deleteCircuitsProvidersId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsProvidersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteCircuitsProvidersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteCircuitsProvidersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimCables - errors', () => {
      it('should have a getDcimCables function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimCables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimCables - errors', () => {
      it('should have a postDcimCables function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimCables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimCables(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimCables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimCables - errors', () => {
      it('should have a putDcimCables function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimCables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimCables(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimCables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimCables - errors', () => {
      it('should have a patchDcimCables function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimCables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimCables(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimCables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimCables - errors', () => {
      it('should have a deleteDcimCables function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimCables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimCablesId - errors', () => {
      it('should have a getDcimCablesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimCablesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimCablesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimCablesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimCablesId - errors', () => {
      it('should have a putDcimCablesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimCablesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimCablesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimCablesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimCablesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimCablesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimCablesId - errors', () => {
      it('should have a patchDcimCablesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimCablesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimCablesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimCablesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimCablesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimCablesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimCablesId - errors', () => {
      it('should have a deleteDcimCablesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimCablesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimCablesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimCablesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConnectedDevice - errors', () => {
      it('should have a getDcimConnectedDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConnectedDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peerDevice', (done) => {
        try {
          a.getDcimConnectedDevice(null, null, (data, error) => {
            try {
              const displayE = 'peerDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimConnectedDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peerInterface', (done) => {
        try {
          a.getDcimConnectedDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'peerInterface is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimConnectedDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleConnections - errors', () => {
      it('should have a getDcimConsoleConnections function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsoleConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePortTemplates - errors', () => {
      it('should have a getDcimConsolePortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsolePortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimConsolePortTemplates - errors', () => {
      it('should have a postDcimConsolePortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimConsolePortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimConsolePortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimConsolePortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsolePortTemplates - errors', () => {
      it('should have a putDcimConsolePortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsolePortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsolePortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimConsolePortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsolePortTemplates - errors', () => {
      it('should have a patchDcimConsolePortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsolePortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsolePortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimConsolePortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsolePortTemplates - errors', () => {
      it('should have a deleteDcimConsolePortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsolePortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePortTemplatesId - errors', () => {
      it('should have a getDcimConsolePortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsolePortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimConsolePortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimConsolePortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsolePortTemplatesId - errors', () => {
      it('should have a putDcimConsolePortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsolePortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimConsolePortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimConsolePortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsolePortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimConsolePortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsolePortTemplatesId - errors', () => {
      it('should have a patchDcimConsolePortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsolePortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimConsolePortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimConsolePortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsolePortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimConsolePortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsolePortTemplatesId - errors', () => {
      it('should have a deleteDcimConsolePortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsolePortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimConsolePortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimConsolePortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePorts - errors', () => {
      it('should have a getDcimConsolePorts function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsolePorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimConsolePorts - errors', () => {
      it('should have a postDcimConsolePorts function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimConsolePorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimConsolePorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimConsolePorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsolePorts - errors', () => {
      it('should have a putDcimConsolePorts function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsolePorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsolePorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimConsolePorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsolePorts - errors', () => {
      it('should have a patchDcimConsolePorts function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsolePorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsolePorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimConsolePorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsolePorts - errors', () => {
      it('should have a deleteDcimConsolePorts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsolePorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePortsId - errors', () => {
      it('should have a getDcimConsolePortsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsolePortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimConsolePortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimConsolePortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsolePortsId - errors', () => {
      it('should have a putDcimConsolePortsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsolePortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimConsolePortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimConsolePortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsolePortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimConsolePortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsolePortsId - errors', () => {
      it('should have a patchDcimConsolePortsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsolePortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimConsolePortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimConsolePortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsolePortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimConsolePortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsolePortsId - errors', () => {
      it('should have a deleteDcimConsolePortsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsolePortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimConsolePortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimConsolePortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePortsIdTrace - errors', () => {
      it('should have a getDcimConsolePortsIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsolePortsIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimConsolePortsIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimConsolePortsIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPortTemplates - errors', () => {
      it('should have a getDcimConsoleServerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsoleServerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimConsoleServerPortTemplates - errors', () => {
      it('should have a postDcimConsoleServerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimConsoleServerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimConsoleServerPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimConsoleServerPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsoleServerPortTemplates - errors', () => {
      it('should have a putDcimConsoleServerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsoleServerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsoleServerPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimConsoleServerPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsoleServerPortTemplates - errors', () => {
      it('should have a patchDcimConsoleServerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsoleServerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsoleServerPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimConsoleServerPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsoleServerPortTemplates - errors', () => {
      it('should have a deleteDcimConsoleServerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsoleServerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPortTemplatesId - errors', () => {
      it('should have a getDcimConsoleServerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsoleServerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimConsoleServerPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimConsoleServerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsoleServerPortTemplatesId - errors', () => {
      it('should have a putDcimConsoleServerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsoleServerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimConsoleServerPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimConsoleServerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsoleServerPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimConsoleServerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsoleServerPortTemplatesId - errors', () => {
      it('should have a patchDcimConsoleServerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsoleServerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimConsoleServerPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimConsoleServerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsoleServerPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimConsoleServerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsoleServerPortTemplatesId - errors', () => {
      it('should have a deleteDcimConsoleServerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsoleServerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimConsoleServerPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimConsoleServerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPorts - errors', () => {
      it('should have a getDcimConsoleServerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsoleServerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimConsoleServerPorts - errors', () => {
      it('should have a postDcimConsoleServerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimConsoleServerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimConsoleServerPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimConsoleServerPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsoleServerPorts - errors', () => {
      it('should have a putDcimConsoleServerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsoleServerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsoleServerPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimConsoleServerPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsoleServerPorts - errors', () => {
      it('should have a patchDcimConsoleServerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsoleServerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsoleServerPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimConsoleServerPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsoleServerPorts - errors', () => {
      it('should have a deleteDcimConsoleServerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsoleServerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPortsId - errors', () => {
      it('should have a getDcimConsoleServerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsoleServerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimConsoleServerPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimConsoleServerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsoleServerPortsId - errors', () => {
      it('should have a putDcimConsoleServerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsoleServerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimConsoleServerPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimConsoleServerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsoleServerPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimConsoleServerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsoleServerPortsId - errors', () => {
      it('should have a patchDcimConsoleServerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsoleServerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimConsoleServerPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimConsoleServerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsoleServerPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimConsoleServerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsoleServerPortsId - errors', () => {
      it('should have a deleteDcimConsoleServerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsoleServerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimConsoleServerPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimConsoleServerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPortsIdTrace - errors', () => {
      it('should have a getDcimConsoleServerPortsIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsoleServerPortsIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimConsoleServerPortsIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimConsoleServerPortsIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceBayTemplates - errors', () => {
      it('should have a getDcimDeviceBayTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceBayTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimDeviceBayTemplates - errors', () => {
      it('should have a postDcimDeviceBayTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimDeviceBayTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimDeviceBayTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimDeviceBayTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceBayTemplates - errors', () => {
      it('should have a putDcimDeviceBayTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceBayTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceBayTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDeviceBayTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceBayTemplates - errors', () => {
      it('should have a patchDcimDeviceBayTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceBayTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceBayTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDeviceBayTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceBayTemplates - errors', () => {
      it('should have a deleteDcimDeviceBayTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceBayTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceBayTemplatesId - errors', () => {
      it('should have a getDcimDeviceBayTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceBayTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimDeviceBayTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimDeviceBayTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceBayTemplatesId - errors', () => {
      it('should have a putDcimDeviceBayTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceBayTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimDeviceBayTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDeviceBayTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceBayTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDeviceBayTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceBayTemplatesId - errors', () => {
      it('should have a patchDcimDeviceBayTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceBayTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimDeviceBayTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDeviceBayTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceBayTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDeviceBayTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceBayTemplatesId - errors', () => {
      it('should have a deleteDcimDeviceBayTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceBayTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimDeviceBayTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimDeviceBayTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceBays - errors', () => {
      it('should have a getDcimDeviceBays function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceBays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimDeviceBays - errors', () => {
      it('should have a postDcimDeviceBays function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimDeviceBays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimDeviceBays(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimDeviceBays', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceBays - errors', () => {
      it('should have a putDcimDeviceBays function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceBays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceBays(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDeviceBays', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceBays - errors', () => {
      it('should have a patchDcimDeviceBays function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceBays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceBays(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDeviceBays', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceBays - errors', () => {
      it('should have a deleteDcimDeviceBays function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceBays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceBaysId - errors', () => {
      it('should have a getDcimDeviceBaysId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceBaysId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimDeviceBaysId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimDeviceBaysId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceBaysId - errors', () => {
      it('should have a putDcimDeviceBaysId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceBaysId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimDeviceBaysId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDeviceBaysId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceBaysId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDeviceBaysId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceBaysId - errors', () => {
      it('should have a patchDcimDeviceBaysId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceBaysId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimDeviceBaysId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDeviceBaysId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceBaysId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDeviceBaysId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceBaysId - errors', () => {
      it('should have a deleteDcimDeviceBaysId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceBaysId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimDeviceBaysId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimDeviceBaysId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceRoles - errors', () => {
      it('should have a getDcimDeviceRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimDeviceRoles - errors', () => {
      it('should have a postDcimDeviceRoles function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimDeviceRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimDeviceRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimDeviceRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceRoles - errors', () => {
      it('should have a putDcimDeviceRoles function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDeviceRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceRoles - errors', () => {
      it('should have a patchDcimDeviceRoles function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDeviceRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceRoles - errors', () => {
      it('should have a deleteDcimDeviceRoles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceRolesId - errors', () => {
      it('should have a getDcimDeviceRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimDeviceRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimDeviceRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceRolesId - errors', () => {
      it('should have a putDcimDeviceRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimDeviceRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDeviceRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDeviceRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceRolesId - errors', () => {
      it('should have a patchDcimDeviceRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimDeviceRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDeviceRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDeviceRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceRolesId - errors', () => {
      it('should have a deleteDcimDeviceRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimDeviceRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimDeviceRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceTypes - errors', () => {
      it('should have a getDcimDeviceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimDeviceTypes - errors', () => {
      it('should have a postDcimDeviceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimDeviceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimDeviceTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimDeviceTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceTypes - errors', () => {
      it('should have a putDcimDeviceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDeviceTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceTypes - errors', () => {
      it('should have a patchDcimDeviceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDeviceTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceTypes - errors', () => {
      it('should have a deleteDcimDeviceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceTypesId - errors', () => {
      it('should have a getDcimDeviceTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimDeviceTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimDeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceTypesId - errors', () => {
      it('should have a putDcimDeviceTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimDeviceTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceTypesId - errors', () => {
      it('should have a patchDcimDeviceTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimDeviceTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceTypesId - errors', () => {
      it('should have a deleteDcimDeviceTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimDeviceTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimDeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDevices - errors', () => {
      it('should have a getDcimDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimDevices - errors', () => {
      it('should have a postDcimDevices function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimDevices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDevices - errors', () => {
      it('should have a putDcimDevices function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDevices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDevices - errors', () => {
      it('should have a patchDcimDevices function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDevices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDevices - errors', () => {
      it('should have a deleteDcimDevices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDevicesId - errors', () => {
      it('should have a getDcimDevicesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDevicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimDevicesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimDevicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDevicesId - errors', () => {
      it('should have a putDcimDevicesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDevicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimDevicesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDevicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDevicesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimDevicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDevicesId - errors', () => {
      it('should have a patchDcimDevicesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDevicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimDevicesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDevicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDevicesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimDevicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDevicesId - errors', () => {
      it('should have a deleteDcimDevicesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDevicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimDevicesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimDevicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDevicesIdNapalm - errors', () => {
      it('should have a getDcimDevicesIdNapalm function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDevicesIdNapalm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimDevicesIdNapalm(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimDevicesIdNapalm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing method', (done) => {
        try {
          a.getDcimDevicesIdNapalm('fakeparam', null, (data, error) => {
            try {
              const displayE = 'method is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimDevicesIdNapalm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPortTemplates - errors', () => {
      it('should have a getDcimFrontPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimFrontPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimFrontPortTemplates - errors', () => {
      it('should have a postDcimFrontPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimFrontPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimFrontPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimFrontPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimFrontPortTemplates - errors', () => {
      it('should have a putDcimFrontPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimFrontPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimFrontPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimFrontPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimFrontPortTemplates - errors', () => {
      it('should have a patchDcimFrontPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimFrontPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimFrontPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimFrontPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimFrontPortTemplates - errors', () => {
      it('should have a deleteDcimFrontPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimFrontPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPortTemplatesId - errors', () => {
      it('should have a getDcimFrontPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimFrontPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimFrontPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimFrontPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimFrontPortTemplatesId - errors', () => {
      it('should have a putDcimFrontPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimFrontPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimFrontPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimFrontPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimFrontPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimFrontPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimFrontPortTemplatesId - errors', () => {
      it('should have a patchDcimFrontPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimFrontPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimFrontPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimFrontPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimFrontPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimFrontPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimFrontPortTemplatesId - errors', () => {
      it('should have a deleteDcimFrontPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimFrontPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimFrontPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimFrontPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPorts - errors', () => {
      it('should have a getDcimFrontPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimFrontPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimFrontPorts - errors', () => {
      it('should have a postDcimFrontPorts function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimFrontPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimFrontPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimFrontPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimFrontPorts - errors', () => {
      it('should have a putDcimFrontPorts function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimFrontPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimFrontPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimFrontPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimFrontPorts - errors', () => {
      it('should have a patchDcimFrontPorts function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimFrontPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimFrontPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimFrontPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimFrontPorts - errors', () => {
      it('should have a deleteDcimFrontPorts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimFrontPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPortsId - errors', () => {
      it('should have a getDcimFrontPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimFrontPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimFrontPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimFrontPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimFrontPortsId - errors', () => {
      it('should have a putDcimFrontPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimFrontPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimFrontPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimFrontPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimFrontPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimFrontPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimFrontPortsId - errors', () => {
      it('should have a patchDcimFrontPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimFrontPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimFrontPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimFrontPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimFrontPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimFrontPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimFrontPortsId - errors', () => {
      it('should have a deleteDcimFrontPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimFrontPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimFrontPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimFrontPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPortsIdPaths - errors', () => {
      it('should have a getDcimFrontPortsIdPaths function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimFrontPortsIdPaths === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimFrontPortsIdPaths(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimFrontPortsIdPaths', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfaceConnections - errors', () => {
      it('should have a getDcimInterfaceConnections function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInterfaceConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfaceTemplates - errors', () => {
      it('should have a getDcimInterfaceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInterfaceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimInterfaceTemplates - errors', () => {
      it('should have a postDcimInterfaceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimInterfaceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimInterfaceTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimInterfaceTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimInterfaceTemplates - errors', () => {
      it('should have a putDcimInterfaceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimInterfaceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimInterfaceTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimInterfaceTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimInterfaceTemplates - errors', () => {
      it('should have a patchDcimInterfaceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimInterfaceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimInterfaceTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimInterfaceTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInterfaceTemplates - errors', () => {
      it('should have a deleteDcimInterfaceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimInterfaceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfaceTemplatesId - errors', () => {
      it('should have a getDcimInterfaceTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInterfaceTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimInterfaceTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimInterfaceTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimInterfaceTemplatesId - errors', () => {
      it('should have a putDcimInterfaceTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimInterfaceTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimInterfaceTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimInterfaceTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimInterfaceTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimInterfaceTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimInterfaceTemplatesId - errors', () => {
      it('should have a patchDcimInterfaceTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimInterfaceTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimInterfaceTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimInterfaceTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimInterfaceTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimInterfaceTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInterfaceTemplatesId - errors', () => {
      it('should have a deleteDcimInterfaceTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimInterfaceTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimInterfaceTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimInterfaceTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfaces - errors', () => {
      it('should have a getDcimInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimInterfaces - errors', () => {
      it('should have a postDcimInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimInterfaces(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimInterfaces - errors', () => {
      it('should have a putDcimInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimInterfaces(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimInterfaces - errors', () => {
      it('should have a patchDcimInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimInterfaces(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInterfaces - errors', () => {
      it('should have a deleteDcimInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfacesId - errors', () => {
      it('should have a getDcimInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimInterfacesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimInterfacesId - errors', () => {
      it('should have a putDcimInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimInterfacesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimInterfacesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimInterfacesId - errors', () => {
      it('should have a patchDcimInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimInterfacesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimInterfacesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInterfacesId - errors', () => {
      it('should have a deleteDcimInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimInterfacesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfacesIdTrace - errors', () => {
      it('should have a getDcimInterfacesIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInterfacesIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimInterfacesIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimInterfacesIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInventoryItems - errors', () => {
      it('should have a getDcimInventoryItems function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInventoryItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimInventoryItems - errors', () => {
      it('should have a postDcimInventoryItems function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimInventoryItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimInventoryItems(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimInventoryItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimInventoryItems - errors', () => {
      it('should have a putDcimInventoryItems function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimInventoryItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimInventoryItems(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimInventoryItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimInventoryItems - errors', () => {
      it('should have a patchDcimInventoryItems function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimInventoryItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimInventoryItems(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimInventoryItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInventoryItems - errors', () => {
      it('should have a deleteDcimInventoryItems function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimInventoryItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInventoryItemsId - errors', () => {
      it('should have a getDcimInventoryItemsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInventoryItemsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimInventoryItemsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimInventoryItemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimInventoryItemsId - errors', () => {
      it('should have a putDcimInventoryItemsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimInventoryItemsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimInventoryItemsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimInventoryItemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimInventoryItemsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimInventoryItemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimInventoryItemsId - errors', () => {
      it('should have a patchDcimInventoryItemsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimInventoryItemsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimInventoryItemsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimInventoryItemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimInventoryItemsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimInventoryItemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInventoryItemsId - errors', () => {
      it('should have a deleteDcimInventoryItemsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimInventoryItemsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimInventoryItemsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimInventoryItemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimManufacturers - errors', () => {
      it('should have a getDcimManufacturers function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimManufacturers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimManufacturers - errors', () => {
      it('should have a postDcimManufacturers function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimManufacturers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimManufacturers(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimManufacturers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimManufacturers - errors', () => {
      it('should have a putDcimManufacturers function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimManufacturers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimManufacturers(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimManufacturers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimManufacturers - errors', () => {
      it('should have a patchDcimManufacturers function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimManufacturers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimManufacturers(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimManufacturers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimManufacturers - errors', () => {
      it('should have a deleteDcimManufacturers function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimManufacturers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimManufacturersId - errors', () => {
      it('should have a getDcimManufacturersId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimManufacturersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimManufacturersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimManufacturersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimManufacturersId - errors', () => {
      it('should have a putDcimManufacturersId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimManufacturersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimManufacturersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimManufacturersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimManufacturersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimManufacturersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimManufacturersId - errors', () => {
      it('should have a patchDcimManufacturersId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimManufacturersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimManufacturersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimManufacturersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimManufacturersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimManufacturersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimManufacturersId - errors', () => {
      it('should have a deleteDcimManufacturersId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimManufacturersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimManufacturersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimManufacturersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPlatforms - errors', () => {
      it('should have a getDcimPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPlatforms - errors', () => {
      it('should have a postDcimPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPlatforms(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPlatforms - errors', () => {
      it('should have a putDcimPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPlatforms(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPlatforms - errors', () => {
      it('should have a patchDcimPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPlatforms(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPlatforms - errors', () => {
      it('should have a deleteDcimPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPlatformsId - errors', () => {
      it('should have a getDcimPlatformsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPlatformsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPlatformsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimPlatformsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPlatformsId - errors', () => {
      it('should have a putDcimPlatformsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPlatformsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPlatformsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPlatformsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPlatformsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPlatformsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPlatformsId - errors', () => {
      it('should have a patchDcimPlatformsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPlatformsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPlatformsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPlatformsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPlatformsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPlatformsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPlatformsId - errors', () => {
      it('should have a deleteDcimPlatformsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPlatformsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPlatformsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimPlatformsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerConnections - errors', () => {
      it('should have a getDcimPowerConnections function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerFeeds - errors', () => {
      it('should have a getDcimPowerFeeds function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerFeeds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPowerFeeds - errors', () => {
      it('should have a postDcimPowerFeeds function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPowerFeeds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPowerFeeds(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimPowerFeeds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerFeeds - errors', () => {
      it('should have a putDcimPowerFeeds function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerFeeds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerFeeds(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerFeeds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerFeeds - errors', () => {
      it('should have a patchDcimPowerFeeds function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerFeeds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerFeeds(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerFeeds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerFeeds - errors', () => {
      it('should have a deleteDcimPowerFeeds function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerFeeds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerFeedsId - errors', () => {
      it('should have a getDcimPowerFeedsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerFeedsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerFeedsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimPowerFeedsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerFeedsId - errors', () => {
      it('should have a putDcimPowerFeedsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerFeedsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPowerFeedsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerFeedsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerFeedsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerFeedsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerFeedsId - errors', () => {
      it('should have a patchDcimPowerFeedsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerFeedsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPowerFeedsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerFeedsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerFeedsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerFeedsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerFeedsId - errors', () => {
      it('should have a deleteDcimPowerFeedsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerFeedsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPowerFeedsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimPowerFeedsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerFeedsIdTrace - errors', () => {
      it('should have a getDcimPowerFeedsIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerFeedsIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerFeedsIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimPowerFeedsIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutletTemplates - errors', () => {
      it('should have a getDcimPowerOutletTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerOutletTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPowerOutletTemplates - errors', () => {
      it('should have a postDcimPowerOutletTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPowerOutletTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPowerOutletTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimPowerOutletTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerOutletTemplates - errors', () => {
      it('should have a putDcimPowerOutletTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerOutletTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerOutletTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerOutletTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerOutletTemplates - errors', () => {
      it('should have a patchDcimPowerOutletTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerOutletTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerOutletTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerOutletTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerOutletTemplates - errors', () => {
      it('should have a deleteDcimPowerOutletTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerOutletTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutletTemplatesId - errors', () => {
      it('should have a getDcimPowerOutletTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerOutletTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerOutletTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimPowerOutletTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerOutletTemplatesId - errors', () => {
      it('should have a putDcimPowerOutletTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerOutletTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPowerOutletTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerOutletTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerOutletTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerOutletTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerOutletTemplatesId - errors', () => {
      it('should have a patchDcimPowerOutletTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerOutletTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPowerOutletTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerOutletTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerOutletTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerOutletTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerOutletTemplatesId - errors', () => {
      it('should have a deleteDcimPowerOutletTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerOutletTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPowerOutletTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimPowerOutletTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutlets - errors', () => {
      it('should have a getDcimPowerOutlets function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerOutlets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPowerOutlets - errors', () => {
      it('should have a postDcimPowerOutlets function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPowerOutlets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPowerOutlets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimPowerOutlets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerOutlets - errors', () => {
      it('should have a putDcimPowerOutlets function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerOutlets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerOutlets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerOutlets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerOutlets - errors', () => {
      it('should have a patchDcimPowerOutlets function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerOutlets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerOutlets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerOutlets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerOutlets - errors', () => {
      it('should have a deleteDcimPowerOutlets function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerOutlets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutletsId - errors', () => {
      it('should have a getDcimPowerOutletsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerOutletsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerOutletsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimPowerOutletsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerOutletsId - errors', () => {
      it('should have a putDcimPowerOutletsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerOutletsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPowerOutletsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerOutletsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerOutletsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerOutletsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerOutletsId - errors', () => {
      it('should have a patchDcimPowerOutletsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerOutletsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPowerOutletsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerOutletsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerOutletsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerOutletsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerOutletsId - errors', () => {
      it('should have a deleteDcimPowerOutletsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerOutletsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPowerOutletsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimPowerOutletsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutletsIdTrace - errors', () => {
      it('should have a getDcimPowerOutletsIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerOutletsIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerOutletsIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimPowerOutletsIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPanels - errors', () => {
      it('should have a getDcimPowerPanels function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPanels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPowerPanels - errors', () => {
      it('should have a postDcimPowerPanels function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPowerPanels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPowerPanels(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimPowerPanels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerPanels - errors', () => {
      it('should have a putDcimPowerPanels function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerPanels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerPanels(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerPanels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerPanels - errors', () => {
      it('should have a patchDcimPowerPanels function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerPanels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerPanels(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerPanels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPanels - errors', () => {
      it('should have a deleteDcimPowerPanels function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerPanels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPanelsId - errors', () => {
      it('should have a getDcimPowerPanelsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPanelsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerPanelsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimPowerPanelsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerPanelsId - errors', () => {
      it('should have a putDcimPowerPanelsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerPanelsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPowerPanelsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerPanelsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerPanelsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerPanelsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerPanelsId - errors', () => {
      it('should have a patchDcimPowerPanelsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerPanelsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPowerPanelsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerPanelsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerPanelsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerPanelsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPanelsId - errors', () => {
      it('should have a deleteDcimPowerPanelsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerPanelsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPowerPanelsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimPowerPanelsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPortTemplates - errors', () => {
      it('should have a getDcimPowerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPowerPortTemplates - errors', () => {
      it('should have a postDcimPowerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPowerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPowerPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimPowerPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerPortTemplates - errors', () => {
      it('should have a putDcimPowerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerPortTemplates - errors', () => {
      it('should have a patchDcimPowerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPortTemplates - errors', () => {
      it('should have a deleteDcimPowerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPortTemplatesId - errors', () => {
      it('should have a getDcimPowerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimPowerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerPortTemplatesId - errors', () => {
      it('should have a putDcimPowerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPowerPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerPortTemplatesId - errors', () => {
      it('should have a patchDcimPowerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPowerPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPortTemplatesId - errors', () => {
      it('should have a deleteDcimPowerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPowerPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimPowerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPorts - errors', () => {
      it('should have a getDcimPowerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPowerPorts - errors', () => {
      it('should have a postDcimPowerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPowerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPowerPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimPowerPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerPorts - errors', () => {
      it('should have a putDcimPowerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerPorts - errors', () => {
      it('should have a patchDcimPowerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPorts - errors', () => {
      it('should have a deleteDcimPowerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPortsId - errors', () => {
      it('should have a getDcimPowerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimPowerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerPortsId - errors', () => {
      it('should have a putDcimPowerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPowerPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimPowerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerPortsId - errors', () => {
      it('should have a patchDcimPowerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPowerPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimPowerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPortsId - errors', () => {
      it('should have a deleteDcimPowerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPowerPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimPowerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPortsIdTrace - errors', () => {
      it('should have a getDcimPowerPortsIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPortsIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerPortsIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimPowerPortsIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackGroups - errors', () => {
      it('should have a getDcimRackGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRackGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRackGroups - errors', () => {
      it('should have a postDcimRackGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRackGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRackGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimRackGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRackGroups - errors', () => {
      it('should have a putDcimRackGroups function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRackGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRackGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRackGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRackGroups - errors', () => {
      it('should have a patchDcimRackGroups function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRackGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRackGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRackGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackGroups - errors', () => {
      it('should have a deleteDcimRackGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRackGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackGroupsId - errors', () => {
      it('should have a getDcimRackGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRackGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRackGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimRackGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRackGroupsId - errors', () => {
      it('should have a putDcimRackGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRackGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRackGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRackGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRackGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRackGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRackGroupsId - errors', () => {
      it('should have a patchDcimRackGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRackGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRackGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRackGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRackGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRackGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackGroupsId - errors', () => {
      it('should have a deleteDcimRackGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRackGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRackGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimRackGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackReservations - errors', () => {
      it('should have a getDcimRackReservations function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRackReservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRackReservations - errors', () => {
      it('should have a postDcimRackReservations function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRackReservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRackReservations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimRackReservations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRackReservations - errors', () => {
      it('should have a putDcimRackReservations function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRackReservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRackReservations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRackReservations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRackReservations - errors', () => {
      it('should have a patchDcimRackReservations function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRackReservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRackReservations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRackReservations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackReservations - errors', () => {
      it('should have a deleteDcimRackReservations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRackReservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackReservationsId - errors', () => {
      it('should have a getDcimRackReservationsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRackReservationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRackReservationsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimRackReservationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRackReservationsId - errors', () => {
      it('should have a putDcimRackReservationsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRackReservationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRackReservationsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRackReservationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRackReservationsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRackReservationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRackReservationsId - errors', () => {
      it('should have a patchDcimRackReservationsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRackReservationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRackReservationsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRackReservationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRackReservationsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRackReservationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackReservationsId - errors', () => {
      it('should have a deleteDcimRackReservationsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRackReservationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRackReservationsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimRackReservationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackRoles - errors', () => {
      it('should have a getDcimRackRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRackRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRackRoles - errors', () => {
      it('should have a postDcimRackRoles function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRackRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRackRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimRackRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRackRoles - errors', () => {
      it('should have a putDcimRackRoles function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRackRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRackRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRackRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRackRoles - errors', () => {
      it('should have a patchDcimRackRoles function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRackRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRackRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRackRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackRoles - errors', () => {
      it('should have a deleteDcimRackRoles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRackRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackRolesId - errors', () => {
      it('should have a getDcimRackRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRackRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRackRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimRackRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRackRolesId - errors', () => {
      it('should have a putDcimRackRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRackRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRackRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRackRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRackRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRackRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRackRolesId - errors', () => {
      it('should have a patchDcimRackRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRackRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRackRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRackRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRackRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRackRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackRolesId - errors', () => {
      it('should have a deleteDcimRackRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRackRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRackRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimRackRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRacks - errors', () => {
      it('should have a getDcimRacks function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRacks - errors', () => {
      it('should have a postDcimRacks function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRacks(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimRacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRacks - errors', () => {
      it('should have a putDcimRacks function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRacks(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRacks - errors', () => {
      it('should have a patchDcimRacks function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRacks(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRacks - errors', () => {
      it('should have a deleteDcimRacks function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRacksId - errors', () => {
      it('should have a getDcimRacksId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRacksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRacksId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimRacksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRacksId - errors', () => {
      it('should have a putDcimRacksId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRacksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRacksId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRacksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRacksId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRacksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRacksId - errors', () => {
      it('should have a patchDcimRacksId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRacksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRacksId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRacksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRacksId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRacksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRacksId - errors', () => {
      it('should have a deleteDcimRacksId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRacksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRacksId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimRacksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRacksIdElevation - errors', () => {
      it('should have a getDcimRacksIdElevation function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRacksIdElevation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRacksIdElevation(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimRacksIdElevation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPortTemplates - errors', () => {
      it('should have a getDcimRearPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRearPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRearPortTemplates - errors', () => {
      it('should have a postDcimRearPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRearPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRearPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimRearPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRearPortTemplates - errors', () => {
      it('should have a putDcimRearPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRearPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRearPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRearPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRearPortTemplates - errors', () => {
      it('should have a patchDcimRearPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRearPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRearPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRearPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRearPortTemplates - errors', () => {
      it('should have a deleteDcimRearPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRearPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPortTemplatesId - errors', () => {
      it('should have a getDcimRearPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRearPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRearPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimRearPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRearPortTemplatesId - errors', () => {
      it('should have a putDcimRearPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRearPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRearPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRearPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRearPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRearPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRearPortTemplatesId - errors', () => {
      it('should have a patchDcimRearPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRearPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRearPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRearPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRearPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRearPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRearPortTemplatesId - errors', () => {
      it('should have a deleteDcimRearPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRearPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRearPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimRearPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPorts - errors', () => {
      it('should have a getDcimRearPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRearPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRearPorts - errors', () => {
      it('should have a postDcimRearPorts function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRearPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRearPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimRearPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRearPorts - errors', () => {
      it('should have a putDcimRearPorts function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRearPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRearPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRearPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRearPorts - errors', () => {
      it('should have a patchDcimRearPorts function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRearPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRearPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRearPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRearPorts - errors', () => {
      it('should have a deleteDcimRearPorts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRearPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPortsId - errors', () => {
      it('should have a getDcimRearPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRearPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRearPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimRearPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRearPortsId - errors', () => {
      it('should have a putDcimRearPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRearPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRearPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRearPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRearPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRearPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRearPortsId - errors', () => {
      it('should have a patchDcimRearPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRearPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRearPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRearPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRearPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRearPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRearPortsId - errors', () => {
      it('should have a deleteDcimRearPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRearPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRearPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimRearPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPortsIdPaths - errors', () => {
      it('should have a getDcimRearPortsIdPaths function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRearPortsIdPaths === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRearPortsIdPaths(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimRearPortsIdPaths', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRegions - errors', () => {
      it('should have a getDcimRegions function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRegions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRegions - errors', () => {
      it('should have a postDcimRegions function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRegions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRegions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimRegions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRegions - errors', () => {
      it('should have a putDcimRegions function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRegions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRegions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRegions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRegions - errors', () => {
      it('should have a patchDcimRegions function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRegions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRegions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRegions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRegions - errors', () => {
      it('should have a deleteDcimRegions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRegions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRegionsId - errors', () => {
      it('should have a getDcimRegionsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRegionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRegionsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimRegionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRegionsId - errors', () => {
      it('should have a putDcimRegionsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRegionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRegionsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRegionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRegionsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimRegionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRegionsId - errors', () => {
      it('should have a patchDcimRegionsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRegionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRegionsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRegionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRegionsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimRegionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRegionsId - errors', () => {
      it('should have a deleteDcimRegionsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRegionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRegionsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimRegionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimSites - errors', () => {
      it('should have a getDcimSites function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimSites - errors', () => {
      it('should have a postDcimSites function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimSites(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimSites - errors', () => {
      it('should have a putDcimSites function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimSites(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimSites - errors', () => {
      it('should have a patchDcimSites function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimSites(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimSites - errors', () => {
      it('should have a deleteDcimSites function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimSitesId - errors', () => {
      it('should have a getDcimSitesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimSitesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimSitesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimSitesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimSitesId - errors', () => {
      it('should have a putDcimSitesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimSitesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimSitesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimSitesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimSitesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimSitesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimSitesId - errors', () => {
      it('should have a patchDcimSitesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimSitesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimSitesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimSitesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimSitesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimSitesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimSitesId - errors', () => {
      it('should have a deleteDcimSitesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimSitesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimSitesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimSitesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimVirtualChassis - errors', () => {
      it('should have a getDcimVirtualChassis function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimVirtualChassis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimVirtualChassis - errors', () => {
      it('should have a postDcimVirtualChassis function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimVirtualChassis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimVirtualChassis(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postDcimVirtualChassis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimVirtualChassis - errors', () => {
      it('should have a putDcimVirtualChassis function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimVirtualChassis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimVirtualChassis(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimVirtualChassis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimVirtualChassis - errors', () => {
      it('should have a patchDcimVirtualChassis function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimVirtualChassis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimVirtualChassis(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimVirtualChassis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimVirtualChassis - errors', () => {
      it('should have a deleteDcimVirtualChassis function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimVirtualChassis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimVirtualChassisId - errors', () => {
      it('should have a getDcimVirtualChassisId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimVirtualChassisId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimVirtualChassisId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getDcimVirtualChassisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimVirtualChassisId - errors', () => {
      it('should have a putDcimVirtualChassisId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimVirtualChassisId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimVirtualChassisId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimVirtualChassisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimVirtualChassisId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putDcimVirtualChassisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimVirtualChassisId - errors', () => {
      it('should have a patchDcimVirtualChassisId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimVirtualChassisId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimVirtualChassisId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimVirtualChassisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimVirtualChassisId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchDcimVirtualChassisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimVirtualChassisId - errors', () => {
      it('should have a deleteDcimVirtualChassisId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimVirtualChassisId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimVirtualChassisId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteDcimVirtualChassisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasConfigContexts - errors', () => {
      it('should have a getExtrasConfigContexts function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasConfigContexts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasConfigContexts - errors', () => {
      it('should have a postExtrasConfigContexts function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasConfigContexts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasConfigContexts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postExtrasConfigContexts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasConfigContexts - errors', () => {
      it('should have a putExtrasConfigContexts function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasConfigContexts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasConfigContexts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasConfigContexts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasConfigContexts - errors', () => {
      it('should have a patchExtrasConfigContexts function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasConfigContexts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasConfigContexts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasConfigContexts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasConfigContexts - errors', () => {
      it('should have a deleteExtrasConfigContexts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasConfigContexts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasConfigContextsId - errors', () => {
      it('should have a getExtrasConfigContextsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasConfigContextsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasConfigContextsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getExtrasConfigContextsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasConfigContextsId - errors', () => {
      it('should have a putExtrasConfigContextsId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasConfigContextsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasConfigContextsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasConfigContextsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasConfigContextsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasConfigContextsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasConfigContextsId - errors', () => {
      it('should have a patchExtrasConfigContextsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasConfigContextsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasConfigContextsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasConfigContextsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasConfigContextsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasConfigContextsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasConfigContextsId - errors', () => {
      it('should have a deleteExtrasConfigContextsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasConfigContextsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasConfigContextsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteExtrasConfigContextsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasContentTypes - errors', () => {
      it('should have a getExtrasContentTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasContentTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasContentTypesId - errors', () => {
      it('should have a getExtrasContentTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasContentTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasContentTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getExtrasContentTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasCustomFields - errors', () => {
      it('should have a getExtrasCustomFields function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasCustomFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasCustomFields - errors', () => {
      it('should have a postExtrasCustomFields function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasCustomFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasCustomFields(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postExtrasCustomFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasCustomFields - errors', () => {
      it('should have a putExtrasCustomFields function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasCustomFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasCustomFields(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasCustomFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasCustomFields - errors', () => {
      it('should have a patchExtrasCustomFields function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasCustomFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasCustomFields(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasCustomFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasCustomFields - errors', () => {
      it('should have a deleteExtrasCustomFields function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasCustomFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasCustomFieldsId - errors', () => {
      it('should have a getExtrasCustomFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasCustomFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasCustomFieldsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getExtrasCustomFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasCustomFieldsId - errors', () => {
      it('should have a putExtrasCustomFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasCustomFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasCustomFieldsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasCustomFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasCustomFieldsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasCustomFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasCustomFieldsId - errors', () => {
      it('should have a patchExtrasCustomFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasCustomFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasCustomFieldsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasCustomFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasCustomFieldsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasCustomFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasCustomFieldsId - errors', () => {
      it('should have a deleteExtrasCustomFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasCustomFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasCustomFieldsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteExtrasCustomFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasExportTemplates - errors', () => {
      it('should have a getExtrasExportTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasExportTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasExportTemplates - errors', () => {
      it('should have a postExtrasExportTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasExportTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasExportTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postExtrasExportTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasExportTemplates - errors', () => {
      it('should have a putExtrasExportTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasExportTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasExportTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasExportTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasExportTemplates - errors', () => {
      it('should have a patchExtrasExportTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasExportTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasExportTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasExportTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasExportTemplates - errors', () => {
      it('should have a deleteExtrasExportTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasExportTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasExportTemplatesId - errors', () => {
      it('should have a getExtrasExportTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasExportTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasExportTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getExtrasExportTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasExportTemplatesId - errors', () => {
      it('should have a putExtrasExportTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasExportTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasExportTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasExportTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasExportTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasExportTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasExportTemplatesId - errors', () => {
      it('should have a patchExtrasExportTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasExportTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasExportTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasExportTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasExportTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasExportTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasExportTemplatesId - errors', () => {
      it('should have a deleteExtrasExportTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasExportTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasExportTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteExtrasExportTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasImageAttachments - errors', () => {
      it('should have a getExtrasImageAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasImageAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasImageAttachments - errors', () => {
      it('should have a postExtrasImageAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasImageAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasImageAttachments(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postExtrasImageAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasImageAttachments - errors', () => {
      it('should have a putExtrasImageAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasImageAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasImageAttachments(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasImageAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasImageAttachments - errors', () => {
      it('should have a patchExtrasImageAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasImageAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasImageAttachments(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasImageAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasImageAttachments - errors', () => {
      it('should have a deleteExtrasImageAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasImageAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasImageAttachmentsId - errors', () => {
      it('should have a getExtrasImageAttachmentsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasImageAttachmentsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasImageAttachmentsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getExtrasImageAttachmentsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasImageAttachmentsId - errors', () => {
      it('should have a putExtrasImageAttachmentsId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasImageAttachmentsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasImageAttachmentsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasImageAttachmentsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasImageAttachmentsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasImageAttachmentsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasImageAttachmentsId - errors', () => {
      it('should have a patchExtrasImageAttachmentsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasImageAttachmentsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasImageAttachmentsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasImageAttachmentsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasImageAttachmentsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasImageAttachmentsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasImageAttachmentsId - errors', () => {
      it('should have a deleteExtrasImageAttachmentsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasImageAttachmentsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasImageAttachmentsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteExtrasImageAttachmentsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasJobResults - errors', () => {
      it('should have a getExtrasJobResults function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasJobResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasJobResultsId - errors', () => {
      it('should have a getExtrasJobResultsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasJobResultsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasJobResultsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getExtrasJobResultsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasObjectChanges - errors', () => {
      it('should have a getExtrasObjectChanges function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasObjectChanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasObjectChangesId - errors', () => {
      it('should have a getExtrasObjectChangesId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasObjectChangesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasObjectChangesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getExtrasObjectChangesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasReports - errors', () => {
      it('should have a getExtrasReports function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasReportsId - errors', () => {
      it('should have a getExtrasReportsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasReportsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasReportsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getExtrasReportsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasReportsIdRun - errors', () => {
      it('should have a postExtrasReportsIdRun function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasReportsIdRun === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postExtrasReportsIdRun(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postExtrasReportsIdRun', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasScripts - errors', () => {
      it('should have a getExtrasScripts function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasScripts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasScriptsId - errors', () => {
      it('should have a getExtrasScriptsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasScriptsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasScriptsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getExtrasScriptsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasTags - errors', () => {
      it('should have a getExtrasTags function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasTags - errors', () => {
      it('should have a postExtrasTags function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasTags(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postExtrasTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasTags - errors', () => {
      it('should have a putExtrasTags function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasTags(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasTags - errors', () => {
      it('should have a patchExtrasTags function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasTags(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasTags - errors', () => {
      it('should have a deleteExtrasTags function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasTagsId - errors', () => {
      it('should have a getExtrasTagsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasTagsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasTagsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getExtrasTagsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasTagsId - errors', () => {
      it('should have a putExtrasTagsId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasTagsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasTagsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasTagsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasTagsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putExtrasTagsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasTagsId - errors', () => {
      it('should have a patchExtrasTagsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasTagsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasTagsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasTagsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasTagsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchExtrasTagsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasTagsId - errors', () => {
      it('should have a deleteExtrasTagsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasTagsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasTagsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteExtrasTagsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAggregates - errors', () => {
      it('should have a getIpamAggregates function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamAggregates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamAggregates - errors', () => {
      it('should have a postIpamAggregates function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamAggregates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamAggregates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamAggregates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamAggregates - errors', () => {
      it('should have a putIpamAggregates function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamAggregates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamAggregates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamAggregates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamAggregates - errors', () => {
      it('should have a patchIpamAggregates function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamAggregates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamAggregates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamAggregates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamAggregates - errors', () => {
      it('should have a deleteIpamAggregates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamAggregates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAggregatesId - errors', () => {
      it('should have a getIpamAggregatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamAggregatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamAggregatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getIpamAggregatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamAggregatesId - errors', () => {
      it('should have a putIpamAggregatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamAggregatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamAggregatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamAggregatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamAggregatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamAggregatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamAggregatesId - errors', () => {
      it('should have a patchIpamAggregatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamAggregatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamAggregatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamAggregatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamAggregatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamAggregatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamAggregatesId - errors', () => {
      it('should have a deleteIpamAggregatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamAggregatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamAggregatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteIpamAggregatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamIpAddresses - errors', () => {
      it('should have a getIpamIpAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamIpAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamIpAddresses - errors', () => {
      it('should have a postIpamIpAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamIpAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamIpAddresses(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamIpAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamIpAddresses - errors', () => {
      it('should have a putIpamIpAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamIpAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamIpAddresses(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamIpAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamIpAddresses - errors', () => {
      it('should have a patchIpamIpAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamIpAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamIpAddresses(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamIpAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamIpAddresses - errors', () => {
      it('should have a deleteIpamIpAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamIpAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamIpAddressesId - errors', () => {
      it('should have a getIpamIpAddressesId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamIpAddressesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamIpAddressesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getIpamIpAddressesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamIpAddressesId - errors', () => {
      it('should have a putIpamIpAddressesId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamIpAddressesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamIpAddressesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamIpAddressesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamIpAddressesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamIpAddressesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamIpAddressesId - errors', () => {
      it('should have a patchIpamIpAddressesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamIpAddressesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamIpAddressesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamIpAddressesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamIpAddressesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamIpAddressesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamIpAddressesId - errors', () => {
      it('should have a deleteIpamIpAddressesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamIpAddressesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamIpAddressesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteIpamIpAddressesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPrefixes - errors', () => {
      it('should have a getIpamPrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamPrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamPrefixes - errors', () => {
      it('should have a postIpamPrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamPrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamPrefixes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamPrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamPrefixes - errors', () => {
      it('should have a putIpamPrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamPrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamPrefixes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamPrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamPrefixes - errors', () => {
      it('should have a patchIpamPrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamPrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamPrefixes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamPrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamPrefixes - errors', () => {
      it('should have a deleteIpamPrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamPrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPrefixesId - errors', () => {
      it('should have a getIpamPrefixesId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamPrefixesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamPrefixesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getIpamPrefixesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamPrefixesId - errors', () => {
      it('should have a putIpamPrefixesId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamPrefixesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamPrefixesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamPrefixesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamPrefixesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamPrefixesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamPrefixesId - errors', () => {
      it('should have a patchIpamPrefixesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamPrefixesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamPrefixesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamPrefixesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamPrefixesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamPrefixesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamPrefixesId - errors', () => {
      it('should have a deleteIpamPrefixesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamPrefixesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamPrefixesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteIpamPrefixesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPrefixesIdAvailableIps - errors', () => {
      it('should have a getIpamPrefixesIdAvailableIps function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamPrefixesIdAvailableIps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamPrefixesIdAvailableIps(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getIpamPrefixesIdAvailableIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamPrefixesIdAvailableIps - errors', () => {
      it('should have a postIpamPrefixesIdAvailableIps function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamPrefixesIdAvailableIps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postIpamPrefixesIdAvailableIps(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamPrefixesIdAvailableIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamPrefixesIdAvailableIps('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamPrefixesIdAvailableIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPrefixesIdAvailablePrefixes - errors', () => {
      it('should have a getIpamPrefixesIdAvailablePrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamPrefixesIdAvailablePrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamPrefixesIdAvailablePrefixes(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getIpamPrefixesIdAvailablePrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamPrefixesIdAvailablePrefixes - errors', () => {
      it('should have a postIpamPrefixesIdAvailablePrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamPrefixesIdAvailablePrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postIpamPrefixesIdAvailablePrefixes(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamPrefixesIdAvailablePrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamPrefixesIdAvailablePrefixes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamPrefixesIdAvailablePrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRirs - errors', () => {
      it('should have a getIpamRirs function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamRirs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamRirs - errors', () => {
      it('should have a postIpamRirs function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamRirs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamRirs(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamRirs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamRirs - errors', () => {
      it('should have a putIpamRirs function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamRirs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamRirs(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamRirs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamRirs - errors', () => {
      it('should have a patchIpamRirs function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamRirs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamRirs(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamRirs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRirs - errors', () => {
      it('should have a deleteIpamRirs function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamRirs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRirsId - errors', () => {
      it('should have a getIpamRirsId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamRirsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamRirsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getIpamRirsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamRirsId - errors', () => {
      it('should have a putIpamRirsId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamRirsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamRirsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamRirsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamRirsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamRirsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamRirsId - errors', () => {
      it('should have a patchIpamRirsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamRirsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamRirsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamRirsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamRirsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamRirsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRirsId - errors', () => {
      it('should have a deleteIpamRirsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamRirsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamRirsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteIpamRirsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRoles - errors', () => {
      it('should have a getIpamRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamRoles - errors', () => {
      it('should have a postIpamRoles function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamRoles - errors', () => {
      it('should have a putIpamRoles function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamRoles - errors', () => {
      it('should have a patchIpamRoles function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRoles - errors', () => {
      it('should have a deleteIpamRoles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRolesId - errors', () => {
      it('should have a getIpamRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getIpamRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamRolesId - errors', () => {
      it('should have a putIpamRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamRolesId - errors', () => {
      it('should have a patchIpamRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRolesId - errors', () => {
      it('should have a deleteIpamRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteIpamRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRouteTargets - errors', () => {
      it('should have a getIpamRouteTargets function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamRouteTargets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamRouteTargets - errors', () => {
      it('should have a postIpamRouteTargets function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamRouteTargets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamRouteTargets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamRouteTargets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamRouteTargets - errors', () => {
      it('should have a putIpamRouteTargets function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamRouteTargets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamRouteTargets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamRouteTargets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamRouteTargets - errors', () => {
      it('should have a patchIpamRouteTargets function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamRouteTargets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamRouteTargets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamRouteTargets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRouteTargets - errors', () => {
      it('should have a deleteIpamRouteTargets function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamRouteTargets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRouteTargetsId - errors', () => {
      it('should have a getIpamRouteTargetsId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamRouteTargetsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamRouteTargetsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getIpamRouteTargetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamRouteTargetsId - errors', () => {
      it('should have a putIpamRouteTargetsId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamRouteTargetsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamRouteTargetsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamRouteTargetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamRouteTargetsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamRouteTargetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamRouteTargetsId - errors', () => {
      it('should have a patchIpamRouteTargetsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamRouteTargetsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamRouteTargetsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamRouteTargetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamRouteTargetsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamRouteTargetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRouteTargetsId - errors', () => {
      it('should have a deleteIpamRouteTargetsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamRouteTargetsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamRouteTargetsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteIpamRouteTargetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamServices - errors', () => {
      it('should have a getIpamServices function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamServices - errors', () => {
      it('should have a postIpamServices function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamServices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamServices - errors', () => {
      it('should have a putIpamServices function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamServices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamServices - errors', () => {
      it('should have a patchIpamServices function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamServices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamServices - errors', () => {
      it('should have a deleteIpamServices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamServicesId - errors', () => {
      it('should have a getIpamServicesId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamServicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamServicesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getIpamServicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamServicesId - errors', () => {
      it('should have a putIpamServicesId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamServicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamServicesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamServicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamServicesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamServicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamServicesId - errors', () => {
      it('should have a patchIpamServicesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamServicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamServicesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamServicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamServicesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamServicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamServicesId - errors', () => {
      it('should have a deleteIpamServicesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamServicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamServicesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteIpamServicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVlanGroups - errors', () => {
      it('should have a getIpamVlanGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamVlanGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamVlanGroups - errors', () => {
      it('should have a postIpamVlanGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamVlanGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamVlanGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamVlanGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamVlanGroups - errors', () => {
      it('should have a putIpamVlanGroups function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamVlanGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamVlanGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamVlanGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamVlanGroups - errors', () => {
      it('should have a patchIpamVlanGroups function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamVlanGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamVlanGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamVlanGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVlanGroups - errors', () => {
      it('should have a deleteIpamVlanGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamVlanGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVlanGroupsId - errors', () => {
      it('should have a getIpamVlanGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamVlanGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamVlanGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getIpamVlanGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamVlanGroupsId - errors', () => {
      it('should have a putIpamVlanGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamVlanGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamVlanGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamVlanGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamVlanGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamVlanGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamVlanGroupsId - errors', () => {
      it('should have a patchIpamVlanGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamVlanGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamVlanGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamVlanGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamVlanGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamVlanGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVlanGroupsId - errors', () => {
      it('should have a deleteIpamVlanGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamVlanGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamVlanGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteIpamVlanGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVlans - errors', () => {
      it('should have a getIpamVlans function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamVlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamVlans - errors', () => {
      it('should have a postIpamVlans function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamVlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamVlans(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamVlans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamVlans - errors', () => {
      it('should have a putIpamVlans function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamVlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamVlans(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamVlans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamVlans - errors', () => {
      it('should have a patchIpamVlans function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamVlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamVlans(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamVlans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVlans - errors', () => {
      it('should have a deleteIpamVlans function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamVlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVlansId - errors', () => {
      it('should have a getIpamVlansId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamVlansId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamVlansId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getIpamVlansId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamVlansId - errors', () => {
      it('should have a putIpamVlansId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamVlansId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamVlansId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamVlansId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamVlansId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamVlansId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamVlansId - errors', () => {
      it('should have a patchIpamVlansId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamVlansId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamVlansId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamVlansId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamVlansId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamVlansId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVlansId - errors', () => {
      it('should have a deleteIpamVlansId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamVlansId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamVlansId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteIpamVlansId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVrfs - errors', () => {
      it('should have a getIpamVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamVrfs - errors', () => {
      it('should have a postIpamVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamVrfs(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postIpamVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamVrfs - errors', () => {
      it('should have a putIpamVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamVrfs(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamVrfs - errors', () => {
      it('should have a patchIpamVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamVrfs(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVrfs - errors', () => {
      it('should have a deleteIpamVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVrfsId - errors', () => {
      it('should have a getIpamVrfsId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamVrfsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamVrfsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getIpamVrfsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamVrfsId - errors', () => {
      it('should have a putIpamVrfsId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamVrfsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamVrfsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamVrfsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamVrfsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putIpamVrfsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamVrfsId - errors', () => {
      it('should have a patchIpamVrfsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamVrfsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamVrfsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamVrfsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamVrfsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchIpamVrfsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVrfsId - errors', () => {
      it('should have a deleteIpamVrfsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamVrfsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamVrfsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteIpamVrfsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretsGenerateRsaKeyPair - errors', () => {
      it('should have a getSecretsGenerateRsaKeyPair function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretsGenerateRsaKeyPair === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretsGetSessionKey - errors', () => {
      it('should have a postSecretsGetSessionKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretsGetSessionKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretsSecretRoles - errors', () => {
      it('should have a getSecretsSecretRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretsSecretRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretsSecretRoles - errors', () => {
      it('should have a postSecretsSecretRoles function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretsSecretRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postSecretsSecretRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postSecretsSecretRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSecretsSecretRoles - errors', () => {
      it('should have a putSecretsSecretRoles function', (done) => {
        try {
          assert.equal(true, typeof a.putSecretsSecretRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putSecretsSecretRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putSecretsSecretRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSecretsSecretRoles - errors', () => {
      it('should have a patchSecretsSecretRoles function', (done) => {
        try {
          assert.equal(true, typeof a.patchSecretsSecretRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchSecretsSecretRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchSecretsSecretRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretsSecretRoles - errors', () => {
      it('should have a deleteSecretsSecretRoles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretsSecretRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretsSecretRolesId - errors', () => {
      it('should have a getSecretsSecretRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretsSecretRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSecretsSecretRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getSecretsSecretRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSecretsSecretRolesId - errors', () => {
      it('should have a putSecretsSecretRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.putSecretsSecretRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putSecretsSecretRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putSecretsSecretRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putSecretsSecretRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putSecretsSecretRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSecretsSecretRolesId - errors', () => {
      it('should have a patchSecretsSecretRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchSecretsSecretRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchSecretsSecretRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchSecretsSecretRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchSecretsSecretRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchSecretsSecretRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretsSecretRolesId - errors', () => {
      it('should have a deleteSecretsSecretRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretsSecretRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteSecretsSecretRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteSecretsSecretRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretsSecrets - errors', () => {
      it('should have a getSecretsSecrets function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretsSecrets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretsSecrets - errors', () => {
      it('should have a postSecretsSecrets function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretsSecrets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postSecretsSecrets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postSecretsSecrets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSecretsSecrets - errors', () => {
      it('should have a putSecretsSecrets function', (done) => {
        try {
          assert.equal(true, typeof a.putSecretsSecrets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putSecretsSecrets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putSecretsSecrets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSecretsSecrets - errors', () => {
      it('should have a patchSecretsSecrets function', (done) => {
        try {
          assert.equal(true, typeof a.patchSecretsSecrets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchSecretsSecrets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchSecretsSecrets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretsSecrets - errors', () => {
      it('should have a deleteSecretsSecrets function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretsSecrets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretsSecretsId - errors', () => {
      it('should have a getSecretsSecretsId function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretsSecretsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSecretsSecretsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getSecretsSecretsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSecretsSecretsId - errors', () => {
      it('should have a putSecretsSecretsId function', (done) => {
        try {
          assert.equal(true, typeof a.putSecretsSecretsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putSecretsSecretsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putSecretsSecretsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putSecretsSecretsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putSecretsSecretsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSecretsSecretsId - errors', () => {
      it('should have a patchSecretsSecretsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchSecretsSecretsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchSecretsSecretsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchSecretsSecretsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchSecretsSecretsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchSecretsSecretsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretsSecretsId - errors', () => {
      it('should have a deleteSecretsSecretsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretsSecretsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteSecretsSecretsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteSecretsSecretsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatus - errors', () => {
      it('should have a getStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGraphql - errors', () => {
      it('should have a getGraphql function', (done) => {
        try {
          assert.equal(true, typeof a.getGraphql === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getGraphql(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getGraphql', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenancyTenantGroups - errors', () => {
      it('should have a getTenancyTenantGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getTenancyTenantGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTenancyTenantGroups - errors', () => {
      it('should have a postTenancyTenantGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postTenancyTenantGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postTenancyTenantGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postTenancyTenantGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTenancyTenantGroups - errors', () => {
      it('should have a putTenancyTenantGroups function', (done) => {
        try {
          assert.equal(true, typeof a.putTenancyTenantGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putTenancyTenantGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putTenancyTenantGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTenancyTenantGroups - errors', () => {
      it('should have a patchTenancyTenantGroups function', (done) => {
        try {
          assert.equal(true, typeof a.patchTenancyTenantGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchTenancyTenantGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchTenancyTenantGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenancyTenantGroups - errors', () => {
      it('should have a deleteTenancyTenantGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTenancyTenantGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenancyTenantGroupsId - errors', () => {
      it('should have a getTenancyTenantGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.getTenancyTenantGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTenancyTenantGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getTenancyTenantGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTenancyTenantGroupsId - errors', () => {
      it('should have a putTenancyTenantGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.putTenancyTenantGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putTenancyTenantGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putTenancyTenantGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putTenancyTenantGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putTenancyTenantGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTenancyTenantGroupsId - errors', () => {
      it('should have a patchTenancyTenantGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchTenancyTenantGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchTenancyTenantGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchTenancyTenantGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchTenancyTenantGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchTenancyTenantGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenancyTenantGroupsId - errors', () => {
      it('should have a deleteTenancyTenantGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTenancyTenantGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteTenancyTenantGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteTenancyTenantGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenancyTenants - errors', () => {
      it('should have a getTenancyTenants function', (done) => {
        try {
          assert.equal(true, typeof a.getTenancyTenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTenancyTenants - errors', () => {
      it('should have a postTenancyTenants function', (done) => {
        try {
          assert.equal(true, typeof a.postTenancyTenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postTenancyTenants(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postTenancyTenants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTenancyTenants - errors', () => {
      it('should have a putTenancyTenants function', (done) => {
        try {
          assert.equal(true, typeof a.putTenancyTenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putTenancyTenants(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putTenancyTenants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTenancyTenants - errors', () => {
      it('should have a patchTenancyTenants function', (done) => {
        try {
          assert.equal(true, typeof a.patchTenancyTenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchTenancyTenants(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchTenancyTenants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenancyTenants - errors', () => {
      it('should have a deleteTenancyTenants function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTenancyTenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenancyTenantsId - errors', () => {
      it('should have a getTenancyTenantsId function', (done) => {
        try {
          assert.equal(true, typeof a.getTenancyTenantsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTenancyTenantsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getTenancyTenantsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTenancyTenantsId - errors', () => {
      it('should have a putTenancyTenantsId function', (done) => {
        try {
          assert.equal(true, typeof a.putTenancyTenantsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putTenancyTenantsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putTenancyTenantsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putTenancyTenantsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putTenancyTenantsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTenancyTenantsId - errors', () => {
      it('should have a patchTenancyTenantsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchTenancyTenantsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchTenancyTenantsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchTenancyTenantsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchTenancyTenantsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchTenancyTenantsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenancyTenantsId - errors', () => {
      it('should have a deleteTenancyTenantsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTenancyTenantsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteTenancyTenantsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteTenancyTenantsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersConfig - errors', () => {
      it('should have a getUsersConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersGroups - errors', () => {
      it('should have a getUsersGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsersGroups - errors', () => {
      it('should have a postUsersGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postUsersGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postUsersGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postUsersGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersGroups - errors', () => {
      it('should have a putUsersGroups function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putUsersGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putUsersGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersGroups - errors', () => {
      it('should have a patchUsersGroups function', (done) => {
        try {
          assert.equal(true, typeof a.patchUsersGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchUsersGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchUsersGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersGroups - errors', () => {
      it('should have a deleteUsersGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersGroupsId - errors', () => {
      it('should have a getUsersGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getUsersGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getUsersGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersGroupsId - errors', () => {
      it('should have a putUsersGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putUsersGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putUsersGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putUsersGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putUsersGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersGroupsId - errors', () => {
      it('should have a patchUsersGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchUsersGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchUsersGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchUsersGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchUsersGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchUsersGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersGroupsId - errors', () => {
      it('should have a deleteUsersGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteUsersGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteUsersGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersPermissions - errors', () => {
      it('should have a getUsersPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsersPermissions - errors', () => {
      it('should have a postUsersPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.postUsersPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postUsersPermissions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postUsersPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersPermissions - errors', () => {
      it('should have a putUsersPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putUsersPermissions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putUsersPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersPermissions - errors', () => {
      it('should have a patchUsersPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.patchUsersPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchUsersPermissions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchUsersPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersPermissions - errors', () => {
      it('should have a deleteUsersPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersPermissionsId - errors', () => {
      it('should have a getUsersPermissionsId function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersPermissionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getUsersPermissionsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getUsersPermissionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersPermissionsId - errors', () => {
      it('should have a putUsersPermissionsId function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersPermissionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putUsersPermissionsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putUsersPermissionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putUsersPermissionsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putUsersPermissionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersPermissionsId - errors', () => {
      it('should have a patchUsersPermissionsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchUsersPermissionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchUsersPermissionsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchUsersPermissionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchUsersPermissionsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchUsersPermissionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersPermissionsId - errors', () => {
      it('should have a deleteUsersPermissionsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersPermissionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteUsersPermissionsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteUsersPermissionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsers - errors', () => {
      it('should have a getUsersUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsersUsers - errors', () => {
      it('should have a postUsersUsers function', (done) => {
        try {
          assert.equal(true, typeof a.postUsersUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postUsersUsers(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postUsersUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersUsers - errors', () => {
      it('should have a putUsersUsers function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putUsersUsers(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putUsersUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersUsers - errors', () => {
      it('should have a patchUsersUsers function', (done) => {
        try {
          assert.equal(true, typeof a.patchUsersUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchUsersUsers(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchUsersUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersUsers - errors', () => {
      it('should have a deleteUsersUsers function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsersId - errors', () => {
      it('should have a getUsersUsersId function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersUsersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getUsersUsersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getUsersUsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersUsersId - errors', () => {
      it('should have a putUsersUsersId function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersUsersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putUsersUsersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putUsersUsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putUsersUsersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putUsersUsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersUsersId - errors', () => {
      it('should have a patchUsersUsersId function', (done) => {
        try {
          assert.equal(true, typeof a.patchUsersUsersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchUsersUsersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchUsersUsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchUsersUsersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchUsersUsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersUsersId - errors', () => {
      it('should have a deleteUsersUsersId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersUsersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteUsersUsersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteUsersUsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusterGroups - errors', () => {
      it('should have a getVirtualizationClusterGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationClusterGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVirtualizationClusterGroups - errors', () => {
      it('should have a postVirtualizationClusterGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postVirtualizationClusterGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postVirtualizationClusterGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postVirtualizationClusterGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationClusterGroups - errors', () => {
      it('should have a putVirtualizationClusterGroups function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationClusterGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationClusterGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationClusterGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationClusterGroups - errors', () => {
      it('should have a patchVirtualizationClusterGroups function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationClusterGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationClusterGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationClusterGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClusterGroups - errors', () => {
      it('should have a deleteVirtualizationClusterGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationClusterGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusterGroupsId - errors', () => {
      it('should have a getVirtualizationClusterGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationClusterGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getVirtualizationClusterGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getVirtualizationClusterGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationClusterGroupsId - errors', () => {
      it('should have a putVirtualizationClusterGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationClusterGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putVirtualizationClusterGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationClusterGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationClusterGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationClusterGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationClusterGroupsId - errors', () => {
      it('should have a patchVirtualizationClusterGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationClusterGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchVirtualizationClusterGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationClusterGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationClusterGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationClusterGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClusterGroupsId - errors', () => {
      it('should have a deleteVirtualizationClusterGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationClusterGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteVirtualizationClusterGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteVirtualizationClusterGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusterTypes - errors', () => {
      it('should have a getVirtualizationClusterTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationClusterTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVirtualizationClusterTypes - errors', () => {
      it('should have a postVirtualizationClusterTypes function', (done) => {
        try {
          assert.equal(true, typeof a.postVirtualizationClusterTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postVirtualizationClusterTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postVirtualizationClusterTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationClusterTypes - errors', () => {
      it('should have a putVirtualizationClusterTypes function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationClusterTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationClusterTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationClusterTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationClusterTypes - errors', () => {
      it('should have a patchVirtualizationClusterTypes function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationClusterTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationClusterTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationClusterTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClusterTypes - errors', () => {
      it('should have a deleteVirtualizationClusterTypes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationClusterTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusterTypesId - errors', () => {
      it('should have a getVirtualizationClusterTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationClusterTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getVirtualizationClusterTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getVirtualizationClusterTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationClusterTypesId - errors', () => {
      it('should have a putVirtualizationClusterTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationClusterTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putVirtualizationClusterTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationClusterTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationClusterTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationClusterTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationClusterTypesId - errors', () => {
      it('should have a patchVirtualizationClusterTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationClusterTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchVirtualizationClusterTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationClusterTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationClusterTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationClusterTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClusterTypesId - errors', () => {
      it('should have a deleteVirtualizationClusterTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationClusterTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteVirtualizationClusterTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteVirtualizationClusterTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusters - errors', () => {
      it('should have a getVirtualizationClusters function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVirtualizationClusters - errors', () => {
      it('should have a postVirtualizationClusters function', (done) => {
        try {
          assert.equal(true, typeof a.postVirtualizationClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postVirtualizationClusters(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postVirtualizationClusters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationClusters - errors', () => {
      it('should have a putVirtualizationClusters function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationClusters(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationClusters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationClusters - errors', () => {
      it('should have a patchVirtualizationClusters function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationClusters(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationClusters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClusters - errors', () => {
      it('should have a deleteVirtualizationClusters function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClustersId - errors', () => {
      it('should have a getVirtualizationClustersId function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationClustersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getVirtualizationClustersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getVirtualizationClustersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationClustersId - errors', () => {
      it('should have a putVirtualizationClustersId function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationClustersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putVirtualizationClustersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationClustersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationClustersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationClustersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationClustersId - errors', () => {
      it('should have a patchVirtualizationClustersId function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationClustersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchVirtualizationClustersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationClustersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationClustersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationClustersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClustersId - errors', () => {
      it('should have a deleteVirtualizationClustersId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationClustersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteVirtualizationClustersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteVirtualizationClustersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationInterfaces - errors', () => {
      it('should have a getVirtualizationInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVirtualizationInterfaces - errors', () => {
      it('should have a postVirtualizationInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.postVirtualizationInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postVirtualizationInterfaces(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postVirtualizationInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationInterfaces - errors', () => {
      it('should have a putVirtualizationInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationInterfaces(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationInterfaces - errors', () => {
      it('should have a patchVirtualizationInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationInterfaces(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationInterfaces - errors', () => {
      it('should have a deleteVirtualizationInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationInterfacesId - errors', () => {
      it('should have a getVirtualizationInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getVirtualizationInterfacesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getVirtualizationInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationInterfacesId - errors', () => {
      it('should have a putVirtualizationInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putVirtualizationInterfacesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationInterfacesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationInterfacesId - errors', () => {
      it('should have a patchVirtualizationInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchVirtualizationInterfacesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationInterfacesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationInterfacesId - errors', () => {
      it('should have a deleteVirtualizationInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteVirtualizationInterfacesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteVirtualizationInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationVirtualMachines - errors', () => {
      it('should have a getVirtualizationVirtualMachines function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationVirtualMachines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVirtualizationVirtualMachines - errors', () => {
      it('should have a postVirtualizationVirtualMachines function', (done) => {
        try {
          assert.equal(true, typeof a.postVirtualizationVirtualMachines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postVirtualizationVirtualMachines(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-postVirtualizationVirtualMachines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationVirtualMachines - errors', () => {
      it('should have a putVirtualizationVirtualMachines function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationVirtualMachines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationVirtualMachines(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationVirtualMachines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationVirtualMachines - errors', () => {
      it('should have a patchVirtualizationVirtualMachines function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationVirtualMachines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationVirtualMachines(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationVirtualMachines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationVirtualMachines - errors', () => {
      it('should have a deleteVirtualizationVirtualMachines function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationVirtualMachines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationVirtualMachinesId - errors', () => {
      it('should have a getVirtualizationVirtualMachinesId function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationVirtualMachinesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getVirtualizationVirtualMachinesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-getVirtualizationVirtualMachinesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationVirtualMachinesId - errors', () => {
      it('should have a putVirtualizationVirtualMachinesId function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationVirtualMachinesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putVirtualizationVirtualMachinesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationVirtualMachinesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationVirtualMachinesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-putVirtualizationVirtualMachinesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationVirtualMachinesId - errors', () => {
      it('should have a patchVirtualizationVirtualMachinesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationVirtualMachinesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchVirtualizationVirtualMachinesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationVirtualMachinesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationVirtualMachinesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-patchVirtualizationVirtualMachinesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationVirtualMachinesId - errors', () => {
      it('should have a deleteVirtualizationVirtualMachinesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationVirtualMachinesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteVirtualizationVirtualMachinesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netbox_v210-adapter-deleteVirtualizationVirtualMachinesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
